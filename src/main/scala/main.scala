import com.github.loki.afro.metallum.entity.partials.PartialDisc
import com.github.loki.afro.metallum.entity.{Band, Disc}
import com.github.loki.afro.metallum.enums.{BandStatus, Country}
import com.github.loki.afro.metallum.search.API
import com.github.loki.afro.metallum.search.query.entity.{BandQuery, SearchBandResult}

import scala.collection.parallel.CollectionConverters.*
import scala.collection.parallel.ParSeq
import scala.jdk.CollectionConverters.*

object main:
  val BAND_STRING  = "steel"
  val TITLE_STRING = "metal"

  case class Catalogue(band: Band, content: Set[String])

  @main def printAlbumResults(): Unit = catalogueForBand(BAND_STRING, TITLE_STRING, albumCatalogue)

  @main def printSongResults(): Unit = catalogueForBand(BAND_STRING, TITLE_STRING, songCatalogue)

  def catalogueForBand(
      bandName: String,
      searchString: String,
      catalogueFunction: (Band, String) => Catalogue
  ): Unit =
    bands(bandName)
      .map(catalogueFunction(_, searchString))
      .map(printableString)
      .foreach(println)

  /** Counts how many bands with the word "steel" in their name have at least one album featuring
    * the word "metal" in it.
    */
  @main def countAlbums(): Unit =
    val response = bands(BAND_STRING).map(albumCatalogue(_, TITLE_STRING))
    val result   = response.count(_.content.nonEmpty)
    println(
      s"Out of ${response.length} bands $result feature at least one album with the word \"metal\" in its title."
    )

  @main def countSongs(): Unit =
    val response = bands(BAND_STRING).map(songCatalogue(_, TITLE_STRING))
    val result   = response.count(_.content.nonEmpty)
    println(
      s"Out of ${response.length} bands $result feature at least one song with the word \"metal\" in its title."
    )

  @main def writeCSVs(): Unit =
    val bs = bands(BAND_STRING)
    println(s"collecting bands done: ${bs.take(3)}")
    val albums = bs.map(albumCatalogue(_, TITLE_STRING))
    println(s"collecting albums done: ${albums.take(3)}")
    val songs = bs.map(songCatalogue(_, TITLE_STRING))
    println(s"collecting songs done: ${songs.take(3)}")
    CSV.writeToCSV(albums.toList, "albums.csv")
    CSV.writeToCSV(songs.toList, "songs.csv")

  def bands(name: String): ParSeq[Band] =
    API
      .getBandsFully(BandQuery.byName(name, false))
      .asScala
      .to(ParSeq)
      .filter(b =>
        Option(b.getName).exists(n =>
          n.toLowerCase().contains(name) // otherwise russian names etc pop up
        )
      )
      .par

  def albumCatalogue(band: Band, name: String): Catalogue =
    val names = band.getDiscsPartial.asScala.toSet
      .map(_.getName)
      .filter(_.toLowerCase().contains(name))
    Catalogue(band, names)

  def songCatalogue(band: Band, name: String): Catalogue =
    val songs = band.getDiscs.asScala
      .flatMap(_.getTrackList.asScala)
      .map(_.getName)
      .filter(_.toLowerCase().contains(name))
      .toSet
    Catalogue(band, songs)

  def printableString(catalogue: Catalogue): String =
    val dString = catalogue.content.toList match
      case Nil => "0"
      case ds  => s"${ds.length}: ${ds.mkString(", ")}"
    s"${catalogue.band.getName}, ${catalogue.band.getCountry}: $dString"

object CSV:
  private val csvSchema = Array("band", "country", "count", "items")

  import au.com.bytecode.opencsv.CSVWriter
  import main.Catalogue

  import java.io.{BufferedWriter, FileWriter}
  import scala.collection.mutable.ListBuffer

  def writeToCSV(data: Seq[Catalogue], fileName: String): Unit =
    val outputFile    = new BufferedWriter(new FileWriter(s"target/$fileName"))
    val csvWriter     = new CSVWriter(outputFile)
    val csvSchema     = Array("band", "country", "count", "items")
    val listOfRecords = new ListBuffer[Array[String]]()
    listOfRecords += csvSchema
    for
      item <- data
      band    = item.band.getName
      country = item.band.getCountry.getShortForm
      count   = item.content.size.toString
      items   = item.content.mkString("; ")
    yield listOfRecords += Array(band, country, count, items)
    csvWriter.writeAll(listOfRecords.asJava)
    outputFile.close()
