# Steel "Data Science"

This little piece of code looks up bands on metal-archives.com that have "steel" in their name (hence the project title) and scans which and how many of their songs & albums have "metal" in their respective names.

Browse the artifacts of the latest run for csv files if you're interested. Those can be imported in your favourite spreadsheet tool, where you can sort them at will.

All made possible by [this metal-archive API library](https://github.com/Loki-Afro/metalarchives).

Have fun!
